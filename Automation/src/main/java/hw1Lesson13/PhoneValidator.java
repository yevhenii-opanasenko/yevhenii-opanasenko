package hw1Lesson13;

import java.util.Scanner;

public class PhoneValidator {

    public static void main(String[] args) {

        String num;
        System.out.print("Enter your phone number");
        Scanner scann = new Scanner(System.in);

        do {
            num = scann.nextLine();
            if (numberCheck(num)) {
                System.out.println("Your phone number has been accepted");
            } else {
                System.out.println("Your phone number is not valid");
                System.out.print("Put the valid number");
            }
        } while (!numberCheck(num));
    }


    public static boolean numberCheck(String s) {
        boolean validNumber;
        String set = "^(\\s*)?([- ()+]?\\d){9,13}";
        validNumber = s.matches(set);
        return validNumber;
    }
}
